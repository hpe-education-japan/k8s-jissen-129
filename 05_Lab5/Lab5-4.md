## Task 4: ConfigMapを使う

**1) ConfigMapを定義します。ConfigMapの定義方法はリテラル設定、ファイルからの読み込み、マニフェストによる方法の3つがあります。まずはファイルを作成しておきましょう。**
```
$ echo blue > blue

$ cat blue
blue

$ echo red > red

$ cat red
red
```

**2) kubectl create コマンドを使ってリテラル、ファイル読み込みでの設定をしてみます。**
```
$ kubectl create configmap colors --from-literal=black=black --from-file=blue --from-file=red

configmap/colors created
```

**3) 作成したConfigMapを確認してみましょう。colorsが今回作成したConfigMapです。**
```
$ kubectl get cm

NAME               DATA   AGE
colors             3      49s
kube-root-ca.crt   1      23h


$ kubectl describe cm colors

Name:         colors
Namespace:    default
Labels:       <none>
Annotations:  <none>

Data
====
black:
----
black
blue:
----
blue

red:
----
red


BinaryData
====

Events:  <none>
```

**4) ConfigMapを使うPodを作成しましょう。EnvFromを使ってみます。**
```
$ vi pod-cm.yaml

```

[pod-cm.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/05_Lab5/Labfiles/pod-cm.yaml)

```
$ kubectl apply -f pod-cm.yaml

pod/cm-pod created


$ kubectl get pods

NAME     READY   STATUS    RESTARTS   AGE
cm-pod   1/1     Running   0          17s
```
**5) 作成したPod内のコンテナでenvコマンドを実行してみます。ConfigMapの内容が定義されています。**
```
$ kubectl exec cm-pod -- env

PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOSTNAME=cm-pod
NGINX_VERSION=1.25.3
NJS_VERSION=0.8.2
PKG_RELEASE=1~bookworm
black=black
blue=blue

red=red
...
```

**6) PodとConfigMapを削除しましょう。**
```
$ kubectl delete pod cm-pod

pod "cm-pod" deleted


$ kubectl delete cm colors

configmap "colors" deleted
```



<br>
<br>
以上でこのタスクは終了です。

[Next](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/05_Lab5/Lab5-5.md)  
[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129)    

