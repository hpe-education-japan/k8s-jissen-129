**apt-get updateで以下のエラーが出た場合**

E: Could not get lock /var/lib/dpkg/lock-frontend. It is held by process 40080 (unattended-upgr)  
N: Be aware that removing the lock file is not a solution and may break your system.  
E: Unable to acquire the dpkg frontend lock (/var/lib/dpkg/lock-frontend), is another process using it?  
student@set0-cp:~$ ls /var/lib/dpkg/lock-frontend.  
ls: cannot access '/var/lib/dpkg/lock-frontend.': No such file or directory  


```
次のように対処してください。

lockファイルの削除

student@set0-cp:~$ sudo rm /var/lib/apt/lists/lock
student@set0-cp:~$ sudo rm /var/lib/dpkg/lock
student@set0-cp:~$ sudo rm /var/lib/dpkg/lock-frontend

student@set0-cp:~$ sudo rm /var/cache/apt/archives/lock
student@set0-cp:~$ sudo apt autoremove

Waiting for cache lock: Could not get lock /var/lib/dpkg/lock-frontend. It is held by process 77262 (unattended-upgr)
Reading package lists... Done
Building dependency tree
Reading state information... Done
0 upgraded, 0 newly installed, 0 to remove and 168 not upgraded.


```
実行後apt-get updateを再実行してください。  

