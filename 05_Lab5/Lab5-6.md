## Task 6: Local Storageを使ったDynamic Provisioningの例 

Local Path Provisionerはローカルストレージを使ったDynamic Provisioningを提供するProvisionerです。  

https://github.com/rancher/local-path-provisioner
<br>
<br>
**1) Provisionerをデプロイします。**
```
$ kubectl apply -f \
https://raw.githubusercontent.com/rancher/local-path-provisioner/v0.0.24/deploy/local-path-storage.yaml

namespace/local-path-storage created
serviceaccount/local-path-provisioner-service-account created
clusterrole.rbac.authorization.k8s.io/local-path-provisioner-role created
clusterrolebinding.rbac.authorization.k8s.io/local-path-provisioner-bind created
deployment.apps/local-path-provisioner created
storageclass.storage.k8s.io/local-path created
configmap/local-path-config created
```

**2) ProvisionerのPodが作成されるのでstatusとlogを確認しましょう。**
```
$ kubectl -n local-path-storage get pod

NAME                                      READY   STATUS    RESTARTS   AGE
local-path-provisioner-8559f79bcf-6d47s   1/1     Running   0          50s


$ kubectl -n local-path-storage logs -f -l app=local-path-provisioner

time="2023-11-28T04:42:26Z" level=debug msg="Applied config: {\"nodePathMap\":[{\"node\":\"DEFAULT_PATH_FOR_NON_LISTED_NODES\",\"paths\":[\"/opt/local-path-provisioner\"]}]}"
time="2023-11-28T04:42:26Z" level=debug msg="Provisioner started"
I1128 04:42:26.752012       1 controller.go:811] Starting provisioner controller rancher.io/local-path_local-path-provisioner-8559f79bcf-6d47s_80b211d5-7050-43b9-8256-68adbc827970!
I1128 04:42:26.853042       1 controller.go:860] Started provisioner controller rancher.io/local-path_local-path-provisioner-8559f79bcf-6d47s_80b211d5-7050-43b9-8256-68ad

"Provisioner started"が確認できたら Ctrl/C で抜けます。
```

**3) Provisionerを使ったPVCを作成します。提供されているサンプルをそのまま使います。**
```
$ kubectl create -f https://raw.githubusercontent.com/rancher/local-path-provisioner/master/examples/pvc/pvc.yaml

persistentvolumeclaim/local-path-pvc created
```
サンプルマニフェスト  
[pvc.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-127/-/blob/master/05_Lab5/Labfiles/pvc-sample.yaml)

**4) PVCを確認します。この時点ではPendingステータスです。**
```
$ kubectl get pvc

NAME             STATUS    VOLUME    CAPACITY   ACCESS MODES   STORAGECLASS   AGE
local-path-pvc   Pending                                       local-path     2m33s
```

**5) PVを確認すると、まだ作成されていません。**
```
$ kubectl get pv

No resources found
```

**6) 作成したPVCを使用するPodを作成します。サンプルを使います。**
```
$ kubectl create -f \
https://raw.githubusercontent.com/rancher/local-path-provisioner/master/examples/pod/pod.yaml

pod/volume-test created
```

サンプルマニフェスト  
[sample.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-127/-/blob/master/05_Lab5/Labfiles/pod-sample.yaml)

```
$ kubectl get pods

NAME          READY   STATUS    RESTARTS   AGE
volume-test   1/1     Running   0          11m
```

**7) PVとPVCを確認します。作成されてBindしています。describeなども確認してみて下さい。**
```
$ kubectl get pv

NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                    STORAGECLASS   REASON   AGE
pvc-f796cf22-31a7-4d01-87d9-87d07e1d5436   128Mi      RWO            Delete           Bound    default/local-path-pvc   local-path              71m


$ kubectl get pvc

NAME             STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
local-path-pvc   Bound    pvc-f796cf22-31a7-4d01-87d9-87d07e1d5436   128Mi      RWO            local-path     83m

```
**8) コンテナに接続してファイル作成など確認してみてください。**
```
$ kubectl exec -ti volume-test -- sh
```

**9) Podを削除します。PVCとPVのステータスを確認しながら削除しましょう。**
```
$ kubectl delete pod volume-test

pod "volume-test" deleted

```

**10) PVCを削除します。**
```
$ kubectl delete pvc local-path-pvc

persistentvolumeclaim "local-path-pvc" deleted
```

**11) PVは自動的に削除されました。**
```
$ kubectl get pv

No resources found
```


<br>
<br>
以上でこのラボは終了です。  

[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129)    

