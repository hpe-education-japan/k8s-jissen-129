## Task 2: hostPath Volume 

**1) hostpathで使用するdirectoryを作成します。**
```
$ mkdir data
```

**2) hostpath Volumeを使用するPodマニフェストを作成します。**
```
$ vi pod-hostpath.yaml
```

[pod-hostpath.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/05_Lab5/Labfiles/pod-hostpath.yaml)

**3) Podを起動します。**
```
$ kubectl apply -f pod-hostpath.yaml

pod/nginx-hostpath created
```

**4) Podのステータスを確認します。**
```
$ kubectl get pods

NAME             READY   STATUS              RESTARTS   AGE
nginx-hostpath   0/1     ContainerCreating   0          39s

```

**5) ContainerCreatingのままのようです。Podの詳細を見てみます。**
```
$ kubectl describe pod nginx-hostpath

Name:             nginx-hostpath
Namespace:        default
Priority:         0
Service Account:  default
Node:             set99-worker/10.0.0.37
Start Time:       Tue, 28 Nov 2023 11:07:50 +0900
Labels:           <none>
Annotations:      <none>
Status:           Pending
...
Events:
  Type     Reason       Age                From               Message
  ----     ------       ----               ----               -------
  Normal   Scheduled    92s                default-scheduler  Successfully assigned default/nginx-hostpath to set99-worker
  Warning  FailedMount  28s (x8 over 92s)  kubelet            MountVolume.SetUp failed for volume "hostpath-volume" : hostPath type check failed: /home/student/data is not a directory

```

Podがワーカーノードで作成されている事がわかります。ワーカーノードではdirectoryを作成していません。ワーカーノードでも作成する必要がありました。

**6) いったん作成中のPodを削除します。**
```
$ kubectl delete pod nginx-hostpath

pod "nginx-hostpath" deleted
```

**7) 別端末でワーカーノードにログインしてdirectoryを作成します。**
```
※ワーカーノードで実行※
tudent@set99-worker:~$ mkdir data
```

**8) コントロールプレーンノードに戻り再度Podを作成します。**
```
※コントロールプレーンノードで実行※
$ kubectl apply -f pod-hostpath.yaml

pod/nginx-hostpath created
```

**9) 今度はRunningになりました。**
```
$ kubectl get pods

NAME             READY   STATUS    RESTARTS   AGE
nginx-hostpath   1/1     Running   0          41s
```

**10) nginx-hostpath Podに接続してmount情報を確認します。**
```
$ kubectl exec -it nginx-hostpath -- /bin/bash

root@nginx-hostpath:/#


root@nginx-hostpath:/# df

Filesystem     1K-blocks    Used Available Use% Mounted on
overlay        103017980 9053020  89544880  10% /
tmpfs              65536       0     65536   0% /dev
/dev/vda2      103017980 9053020  89544880  10% /test-data
...
```

**11) /test-dataにファイルを作成します。exitもしておきましょう。**
```
root@nginx-hostpath:/# touch /test-data/file

root@nginx-hostpath:/# ls /test-data
file

root@nginx-hostpath:/# exit

```

**12 ワーカーノードからPodで作成したファイルを確認します。**
```
※ワーカーノードで実行※
student@set99-worker:~$ ls -la data

total 8
drwxrwxr-x 2 student student 4096 Nov 28 11:15 .
drwxr-xr-x 5 student student 4096 Nov 28 11:12 ..
-rw-r--r-- 1 root    root       0 Nov 28 11:15 file

```
このようにhostpathはPodが起動するノードのdirectoryを使用するので注意が必要です。
<br>
<br>

**13) 作成したPodを削除しましょう。**
```
※コントロールプレーンノードで実行※
$ kubectl delete pod nginx-hostpath

pod "nginx-hostpath" deleted
```



<br>
<br>
以上でこのタスクは終了です。

[Next](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/05_Lab5/Lab5-3.md)  
[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129)    

