# 5章 Volume

**時間:40分**

**目的:**

このラボを完了すると、次のことができるようになります。  
・Volumeを使う  
・PersistentVolumeを作成して使用する  
・PersistentVolumeClaimを作成して使用する  
・ConfigMapを使う  
・Secretを使う  
・Dynamic Provisioningの理解  


<br>
<br>


## Task1: emptyDirを使ったPod内コンテナのファイル共有

<br>
<br>

![Lab5-1](/uploads/8884a3e1a5a2457aa1ad302ab3fd5671/Lab5-1.png)

<br>
<br>

**1) emptyDirを共有するPodのマニフェストを作成します。redisコンテナでは/data/redis, nginxコンテナでは/usr/share/nginx/htmlにマウントします。**
```
$ vi pod-storage.yaml

```
[pod-storage.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/05_Lab5/Labfiles/pod-storage.yaml)

**2) Podを起動します。**
```
$ kubectl apply -f pod-storage.yaml

pod/two-containers created

```

**3) Podを確認します。**
```
$ kubectl get pods

NAME             READY   STATUS    RESTARTS   AGE
two-containers   2/2     Running   0          39s


$ kubectl describe pod two-containers

Name:             two-containers
Namespace:        default
Priority:         0
Service Account:  default
...
Containers:
  redis:
    Container ID:   containerd://8b9e20b8fa82a9b065f4625d07b2c3c338a53ca6585d8fe00c430af72c1b437e
    Image:          redis
    Image ID:       docker.io/library/redis@sha256:2976bc0437deff693af2dcf081a1d1758de8b413e6de861151a5a136c25eb9e4
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Tue, 28 Nov 2023 10:36:09 +0900
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /data/redis from redis-storage (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-wrz5h (ro)
  nginx:
    Container ID:   containerd://3c070e7f6577373de476ccc3ec28d77799ef9b69fbd95bd276ec8d64d2d5e0f1
    Image:          nginx
    Image ID:       docker.io/library/nginx@sha256:10d1f5b58f74683ad34eb29287e07dab1e90f10af243f151bb50aa5dbb4d62ee
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Tue, 28 Nov 2023 10:36:10 +0900
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /usr/share/nginx/html from redis-storage (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-wrz5h (ro)

```

**4) redisコンテナでシェルを実行します。コンテナ内で実行されているためプロンプトが変ります。**
```
$ kubectl exec -it two-containers -c redis -- /bin/bash

root@two-containers:/data# 
```

**5) コンテナ内でファイルを作成します。**
```
root@two-containers:/data# cd /data/redis

root@two-containers:/data/redis# ls

root@two-containers:/data/redis# echo "from redis" > from-redis

root@two-containers:/data/redis# cat from-redis
from redis

```

**6) コンテナからexitします。**
```
root@two-containers:/data/redis# exit
```

**7) nginxコンテナでシェルを実行します。**
```
$ kubectl exec -it two-containers -c nginx -- /bin/bash

root@two-containers:/#

```

**8) ファイルを確認します。**
```
root@two-containers:/# cd /usr/share/nginx/html


root@two-containers:/usr/share/nginx/html# ls

from-redis

root@two-containers:/usr/share/nginx/html# cat from-redis

from redis

```

**9) コンテナからexitします。**
```
root@two-containers:/usr/share/nginx/html# exit
```

**10) Podを削除しましょう。**
```
$ kubectl delete pod two-containers

pod "two-containers" deleted
```



<br>
<br>
以上でこのタスクは終了です。

[Next](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/05_Lab5/Lab5-2.md)  
[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129)    





