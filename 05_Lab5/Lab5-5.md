## Task 5: Secretを使う

**1) Secretを定義します。リテラルで設定しましょう。**
```
$ kubectl create secret generic app-secret --from-literal=DB_host=mysql --from-literal=DB_user=root --from-literal=DB_Password=password

secret/app-secret created
```

**2) Secretを確認します。-o yamlで確認するとエンコードした値が表示されます。**
```
$ kubectl get secrets

NAME         TYPE     DATA   AGE
app-secret   Opaque   3      34s


$ kubectl describe secrets app-secret

Name:         app-secret
Namespace:    default
Labels:       <none>
Annotations:  <none>

Type:  Opaque

Data
====
DB_host:      5 bytes
DB_user:      4 bytes
DB_Password:  7 bytes


$ kubectl get secrets app-secret -o yaml

apiVersion: v1
data:
  DB_Password: cGFzc3dvcg==
  DB_host: bXlzcWw=
  DB_user: cm9vdA==
kind: Secret
metadata:
  creationTimestamp: "2023-11-28T04:25:21Z"
  name: app-secret
  namespace: default
  resourceVersion: "41455"
  uid: 1d2502be-966c-48e5-9e3f-427e139050b8
type: Opaque

```

**3) Secretを使用するredis Podを作成して、パスワードをSecretを使って受け渡します。マニフェストを作成しましょう。valueFromを使って受け渡します。**
```
$ vi redis-pod.yaml
```

[redis-pod.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/05_Lab5/Labfiles/db-pod.yaml)

```
$ kubectl apply -f redis-pod.yaml

pod/redis-pod created


$ kubectl get pods

NAME        READY   STATUS    RESTARTS   AGE
redis-pod   1/1     Running   0          9s
```

**4) 作成したredis Podに接続して動作を確認してみましょう。**
```
$ kubectl exec -ti redis-pod -- /bin/bash

root@redis-pod:/data#
root@redis-pod:/data# echo $DB_PASSWORD
password

root@redis-pod:/data# exit

```

**5) DeploymentとSecretを削除しておきましょう。**
```
$ kubectl delete secret app-secret

secret "app-secret" deleted


$ kubectl delete pod redis-pod

pod "redis-pod" deleted
```



<br>
<br>
以上でこのタスクは終了です。

[Next](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/05_Lab5/Lab5-6.md)  
[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129)    
