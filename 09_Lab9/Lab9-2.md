## Task 2: PodSecurity設定

**1) まず何も設定せずにbusybox Podを作成します。**
```
$ kubectl run busybox --image=busybox -- sleep 1h

pod/busybox created


$ kubectl get pods

NAME      READY   STATUS    RESTARTS   AGE
busybox   1/1     Running   0          24s
```

**2) Podに接続してユーザーIDを確認します。rootユーザーで動いている事がわかります。**
```
$ kubectl exec -ti busybox -- sh

/ # id
uid=0(root) gid=0(root) groups=0(root),10(wheel)

/ # ps
PID   USER     TIME  COMMAND
    1 root      0:00 sleep 1h
    7 root      0:00 sh
   14 root      0:00 ps

/ # exit
```

**3) 別のPodをsecurityContextを使って起動します。マニフェストテンプレートを作成しましょう。**
```
$ kubectl run busybox2 --image=busybox --dry-run=client -o yaml -- sleep 1h > busybox2.yaml
```

**4) securityContextを設定します。**
```
$ vi busybox2.yaml

<省略>
spec:
  securityContext:
    runAsUser: 1000
    runAsGroup: 3000
<省略>

```

[busybox2.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/09_Lab9/Labfiles/busybox2.yaml)

**5) Podを起動します。**
```
$ kubectl create -f busybox2.yaml


$ kubectl get pods

NAME       READY   STATUS    RESTARTS   AGE
busybox    1/1     Running   0          6m28s
busybox2   1/1     Running   0          36s
```

**6) 同じようにPodに接続してユーザーを確認します。設定したUID、GIDになっています。**
```
$ kubectl exec -ti busybox2 -- sh

~ $ id
uid=1000 gid=3000 groups=3000

~ $ ps
PID   USER     TIME  COMMAND
    1 1000      0:00 sleep 1h
    7 1000      0:00 sh
   14 1000      0:00 ps
```

**7) Podからexitして、Podを削除しておきましょう。**
```
~ $ exit

$ kubectl delete pods --all

```

<br>
<br>
以上でこのタスクは終了です。

[Next](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/09_Lab9/Lab9-3.md)  
[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129) 
