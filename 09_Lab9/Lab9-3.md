## Task 3: Network Security Policy設定
<br>
<br>
![Lab9-3](/uploads/66f7c160b86e77980c57e894a7f44081/Lab9-3.png)
<br>
<br>

全ての通信を遮断するAll denyのポリシーを適用して、必要な通信のみを許可するポリシーを定義していきます。nginx2からnginx1への通信のみを許可するように設定します。

**1) nginx podを3つ立ち上げておきます。**
```
$ kubectl run nginx1 --image=nginx
$ kubectl run nginx2 --image=nginx
$ kubectl run nginx3 --image=nginx

```

**2) それぞれのLabelとIPアドレスを確認しておきましょう。**
```
$ kubectl get pods --show-labels

NAME          READY   STATUS    RESTARTS   AGE   LABELS
nginx1         1/1    Running   0          52s   run=nginx1
nginx2         1/1    Running   0          48s   run=nginx2
nginx3         1/1    Running   0          44s   run=nginx3


$ kubectl get pods -o wide

NAME          READY   STATUS    RESTARTS   AGE     IP           NODE           NOMINATED NODE   READINESS GATES
nginx1        1/1     Running   0          111s    10.244.0.7   set99-worker   <none>           <none>
nginx2        1/1     Running   0          107s    10.244.0.8   set99-worker   <none>           <none>
nginx3        1/1     Running   0          103s    10.244.0.9   set99-worker   <none>           <none>

```

**3) それぞれNetworkアクセスが可能な事を確認しておきます。**
```
$ kubectl exec -ti nginx1 -- /bin/bash

root@nginx1:/# curl 10.244.0.8

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<省略>

root@nginx1:/# exit


$ kubectl exec -ti nginx2 -- /bin/bash

root@nginx2:/# curl 10.244.0.7

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<省略>

root@nginx2:/# exit


$ kubectl exec -ti nginx3 -- /bin/bash

root@nginx3:/# curl 10.244.0.7

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<省略>

root@nginx3:/# exit

```

**4) 最初にAll denyのNetworkPolicyを定義します。マニフェストを作成しましょう。**
```
$ vi np-all-deny.yaml
```

[np-all-deny.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/09_Lab9/Labfiles/np-all-deny.yaml)
<br>
<br>

**5) 適用します。**
```
$ kubectl create -f np-all-deny.yaml

networkpolicy.networking.k8s.io/default-deny created
```

**6) Networkアクセスが遮断された事を確認します。**
```
$ kubectl exec -ti nginx1 -- /bin/bash

root@nginx1:/# curl 10.244.0.8
^C で終了してください。

root@nginx1:/# exit

```

**7) ここからアクセス要件に従ってPolicyを定義します。ここでは、nginx2からnginx1へのアクセスができるように設定します。その為にはnginx1のIngreee、nginx2のEgressを許可する必要があります。それぞれのマニフェストを作成しましょう。**
```
$ vi np-nginx1.yaml
```

[np-nginx1.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/09_Lab9/Labfiles/np-nginx1.yaml)


```
$ vi np-nginx2.yaml
```

[np-nginx2.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/09_Lab9/Labfiles/np-nginx2.yaml)
<br>
<br>


**8) 適用します。**
```
$ kubectl create -f np-nginx1.yaml

networkpolicy.networking.k8s.io/np-nginx1 created


$ kubectl create -f np-nginx2.yaml

networkpolicy.networking.k8s.io/np-nginx2 created
```

**9) アクセスを確認しましょう。通信ができるようになりました。**
```
$ kubectl exec -ti nginx2 -- /bin/bash

root@nginx2:/# curl 10.244.0.7
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<省略>

root@nginx2:/# exit
```

**10) 他のPodからのアクセスを確認しましょう。通信はできません。**
```
$ kubectl exec -ti nginx3 -- /bin/bash

root@nginx3:/# curl 10.244.0.7
^C

root@nginx3:/# exit

```

**11) PodとNetworkPolicyを削除しましょう。**
```
$ kubectl delete networkpolicies.networking.k8s.io --all

networkpolicy.networking.k8s.io "default-deny" deleted
networkpolicy.networking.k8s.io "np-nginx1" deleted
networkpolicy.networking.k8s.io "np-nginx2" deleted


$ kubectl delete pod --all

pod "nginx1" deleted
pod "nginx2" deleted
pod "nginx3" deleted

```


<br>
<br>
以上でこのラボは終了です。

[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129) 
