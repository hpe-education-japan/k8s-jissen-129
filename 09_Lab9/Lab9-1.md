# 9章 Security

**時間:40分**

**目的:**

このラボを完了すると、次のことができるようになります。  
・認証設定  
・RBAC設定  
・PodSecurity設定  
・NetworkPolicy設定  


<br>
<br>


## Task1: 認証設定と認可設定

この演習では例として新規メンバーtomさんがKubernetesのPod情報を取得できるように認証とRBACの設定を行います。

**1) Podを1個起動しておきましょう。**
```
$ kubectl run nginx --image=nginx
$ kubectl get pods
```

**2) tomユーザーを追加しましょう。その後パスワード設定をして確認します。この作業は一般的なLinuxのユーザー追加作業です。パスワードはpasswordとします。**
```
$ sudo useradd -m -s /bin/bash tom


$ sudo passwd tom

New password: password
Retype new password: password
passwd: password updated successfully


$ su - tom

tom@set99-cp:~$ pwd
/home/tom


tom@set99-cp:~$ exit

```

**3) tomをkubernetes管理者として設定します。まずは認証情報を設定しましょう。プライベートキーとCertificate Signing Request (CSR)を作成します。  
最初にプライベートキーを作成します。**
```
$ openssl genrsa -out tom.key 2048

``` 
  
**次にCSRを作成します。**
<br>

```
$ openssl req -new -key tom.key -out tom.csr -subj "/CN=tom/O=development"

```

**4) 作成したCSR ファイルを利用し、x509 プロトコルで自己署名証明書を生成しましょう。Kubernetes クラスタのCA キーを利用し、45 日間の有効期限を設定します。**
```
$ sudo openssl x509 -req -in tom.csr -CA /etc/kubernetes/pki/ca.crt -CAkey /etc/kubernetes/pki/ca.key \
-CAcreateserial -out tom.crt -days 45

Signature ok
subject=CN = tom, O = development
Getting CA Private Key
```
**5) ここからKubernetes側の作業です。kubeconfigにユーザー認証情報を登録します。**  
<br>
<br>
![Lab9-1](/uploads/d75490410eeb03ca5ae12c2aeaf435f3/Lab9-1.png)
<br>
<br>
```
$ kubectl config set-credentials tom --client-certificate=/home/student/tom.crt --client-key=/home/student/tom.key

User "tom" set.
```

**6) 登録できたか確認しましょう。**
```
$ kubectl config view

...
- name: tom
  user:
    client-certificate: /home/student/tom.crt
    client-key: /home/student/tom.key

```

**7) kubeconfigにtom用のcontextを設定します。**
```
$ kubectl config set-context tom-context --cluster=kubernetes --user=tom

Context "tom-context" created.
```

**8) 設定できたか確認しましょう。**
```
$ kubectl config view

...
- context:
    cluster: kubernetes
    user: tom
  name: tom-context
...
```

**9) tom-context指定により、登録したtom用認証情報を使って、Kubernetesにアクセスできるか確認します。**
```
$ kubectl --context=tom-context get pods

Error from server (Forbidden): pods is forbidden: User "tom" cannot list resource "pods" in API group "" in the namespace "default"
```

**10) エラーを確認すると認証は通っていますがPod情報を取得する権限が無いようです。RBACを設定しましょう。**  
<br>
<br>
![Lab9-2](/uploads/426ad8029836824404b049ae5864f968/Lab9-2.png)
<br>
<br>
  
**11) まずPodの情報を取得できるpod-readerロールを作成します。ここではCLIを使います。**
```
$ kubectl create role pod-reader --verb=get --verb=list --verb=watch --resource=pods

role.rbac.authorization.k8s.io/pod-reader created


$ kubectl describe role pod-reader

Name:         pod-reader
Labels:       <none>
Annotations:  <none>
PolicyRule:
  Resources  Non-Resource URLs  Resource Names  Verbs
  ---------  -----------------  --------------  -----
  pods       []                 []              [get list watch]
```

**12) pod-reader roleとtomユーザーをバインドするrolebindingを作成します。**
```
$ kubectl create rolebinding tom-binding --role=pod-reader --user=tom

rolebinding.rbac.authorization.k8s.io/tom-binding created


$ kubectl describe rolebindings tom-binding

Name:         tom-binding
Labels:       <none>
Annotations:  <none>
Role:
  Kind:  Role
  Name:  pod-reader
Subjects:
  Kind  Name  Namespace
  ----  ----  ---------
  User  tom

```

**13) それではもう一度tom-contextでtomユーザーがPod情報を取得できるか確認しましょう。**
```
$ kubectl --context=tom-context get pods

NAME    READY   STATUS    RESTARTS   AGE
nginx   1/1     Running   0          21m
```

**14) これでtomユーザーはPod情報を取得できるようになりました。今の状態は/home/studentの下に作成したtom用証明書とキー、 .kube/configファイルを使っています。実際はtomユーザーはログインしてKubernetesリソースにアクセスします。やってみると、tomユーザーのログインディレクトリに. kube/configがないのでエラーになります。証明書やキーもtomのhome directoryに存在する必要があります。**
```
$ su - tom


$ kubectl get pods

E1130 10:58:20.917877  144949 memcache.go:265] couldn't get current server API group list: Get "http://localhost:8080/api?timeout=32s": dial tcp 127.0.0.1:8080: connect: connection refused
The connection to the server localhost:8080 was refused - did you specify the right host or port?


$ exit
```
**15) 先ほど作成したキー、証明書、.kube/configをtomのhome directoryにコピーして、オーナー変更、configファイルの修正を行います。  
まず証明書とキーファイルをコピーしてオーナー変更を行います。**
```
$ sudo cp /home/student/tom.* /home/tom/
$ sudo chown tom:tom /home/tom/tom.crt
$ sudo chown tom:tom /home/tom/tom.csr
$ sudo chown tom:tom /home/tom/tom.key

```

次に .kube/config ファイルをコピーします。directory作成から行います。  
```
$ sudo mkdir /home/tom/.kube
$ sudo chown tom:tom  /home/tom/.kube
$ sudo cp /home/student/.kube/config /home/tom/.kube/
$ sudo chown tom:tom /home/tom/.kube/config

```

/home/tom/.kube/configファイルを更新します。
```
$ sudo vi /home/tom/.kube/config

apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: 

<省略>

    server: https://k8scp:6443
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: kubernetes-admin
  name: kubernetes-admin@tom
- context:
    cluster: kubernetes
    user: tom
  name: tom-context
current-context: tom-context　   #<-Corrent contextをtom-contextに変更
kind: Config
preferences: {}
users:
- name: kubernetes-admin
  user:
    client-certificate-data: 

<省略>

    client-key-data: 

<省略>

- name: tom
  user:
    client-certificate: /home/tom/tom.crt　 #<- tomのhomeにコピーした認証情報に変更
    client-key: /home/tom/tom.key　         #<- tomのhomeにコピーしたキーに変更

```

**16) tomユーザーでkubectlコマンドで確認しましょう。**
```
$ su - tom

tom@set99-cp:~$ kubectl get pods

NAME    READY   STATUS    RESTARTS   AGE
nginx   1/1     Running   0          29m

```
<br>

**これでtomユーザーはログインしてPod情報を取得できるようになりました。**

<br>

**17) オプション演習：Podの削除はできません。時間があればRBACの設定を変更してtomユーザーにPod削除権限を付けてみましょう。**
```
tom@set99-cp:~$ kubectl delete pod nginx
Error from server (Forbidden): pods "nginx" is forbidden: User "tom" cannot delete resource "pods" in API group "" in the namespace "default"
```

**18) tomユーザーからexitしてPodを削除しておきましょう。**
```
tom@set99-cp:~$ exit

$ kubectl delete pods --all

```
<br>
<br>
以上でこのタスクは終了です。

[Next](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/09_Lab9/Lab9-2.md)  
[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129) 

