# 7章 Ingress

**時間:30分**

**目的:**

このラボを完了すると、次のことができるようになります。  
・Ingress Controllerを理解する  
・Ingressリソースを作成する


<br>
<br>


## Task1: Ingress Controllerのインストール

この演習ではNginx Ingress Controllerをインストールします。一般的なインストールはhelmを使うので最初にhelmをインストールします。  
<br>
<br>
**1) helmをインストールします。**
```
$ wget https://get.helm.sh/helm-v3.9.2-linux-amd64.tar.gz

--2023-11-28 15:33:47--  https://get.helm.sh/helm-v3.9.2-linux-amd64.tar.gz
Resolving get.helm.sh (get.helm.sh)... 152.199.39.108, 2606:2800:247:1cb7:261b:1f9c:2074:3c
Connecting to get.helm.sh (get.helm.sh)|152.199.39.108|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 13998934 (13M) [application/x-tar]
Saving to: ‘helm-v3.9.2-linux-amd64.tar.gz’

helm-v3.9.2-linux-amd64.tar.gz             100%
[======================================================================================>]  13.35M  --.-KB/s    in 0.09s

2023-11-28 15:33:47 (148 MB/s) - ‘helm-v3.9.2-linux-amd64.tar.gz’ saved [13998934/13998934]


$ tar -xvf helm-v3.9.2-linux-amd64.tar.gz

linux-amd64/
linux-amd64/helm
linux-amd64/LICENSE
linux-amd64/README.md


$ sudo cp linux-amd64/helm /usr/local/bin/helm

```

helmがインストールされhelmコマンドが使えるようになりました。  

**2) Nginx Ingress controllerリポジトリを追加してupdateします。**
```
$ helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx

"ingress-nginx" has been added to your repositories


$ helm repo update

Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "ingress-nginx" chart repository
...Successfully got an update from the "nginx-stable" chart repository
Update Complete. ?Happy Helming!?

```

**3) Ingress controllerをインストールします。まずIngress controller用のNamespaceを作成します。**
```
$ kubectl create ns ingress

namespace/ingress created
```

**4) Ingress helm chartをプルします。**
```
$ helm fetch ingress-nginx/ingress-nginx --untar
```

**5) Chartのvalues.yamlを変更します。218行目付近のkind: Deploymentをkind: DaemonSetに変更します。**
```
$ cd ingress-nginx/
$ vi values.yaml


# -- Use a `DaemonSet` or `Deployment`
kind: DaemonSet   # ←DeploymentをDaemonSetに変更
# -- Annotations to be added to the controller Deployment or DaemonSet
##

```

**6 ) ingress namespaceにIngress controllerをインストールします。**
```
$ helm install -n ingress myingress .

NAME: myingress
LAST DEPLOYED: Wed Apr  3 13:27:56 2024
NAMESPACE: ingress
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
The ingress-nginx controller has been installed…..

```

**7) Ingress Controller PodとServiceを確認します。**
```
$ kubectl -n ingress get ds

NAME                                 DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
myingress-ingress-nginx-controller   2         2         2       2            2     
    kubernetes.io/os=linux    10m


$ kubectl -n ingress get pods

NAME                                       READY   STATUS    RESTARTS   AGE
myingress-ingress-nginx-controller-b4czj   1/1     Running    0         12m
myingress-ingress-nginx-controller-fnlkr   1/1     Running    0         12m




$ kubectl -n ingress get svc

NAME                                           TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)             AGE
myingress-ingress-nginx-controller             LoadBalancer   10.102.12.45     <pending>     80:31019/TCP,443:31546/TCP   15m
myingress-ingress-nginx-controller-admission   ClusterIP      10.101.125.128   <none>        443/TCP             15m

```

Ingress controllerがインストールされました。Homeディレクトリに戻っておきましょう。
```
$ cd ~
```

<br>
<br>
以上でこのタスクは終了です。

[Next](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/07_Lab7/Lab7-2.md)  
[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129)    



