## Task 2: Ingress の利用
<br>
Serviceを2つ作成してIngressによる転送を行います。www.web1.external.comがweb1サービス、www.web2.external.comがweb2サービスに転送されるようにingress ruleを設定します。
<br>
<br>
![Lab7-1](/uploads/35fe7a52e774826f2b890d68c3d950e8/Lab7-1.png)
<br>
<br>

**1) アプリケーションとして、2つのnginxをデプロイします。レプリカ数は2個にします。**
```
$ kubectl create deployment web1 --image=nginx --replicas 2

deployment.apps/web1 created


$ kubectl expose deployment web1 --port 80

service/web1 exposed


$ kubectl create deployment web2 --image=nginx --replicas 2

deployment.apps/web2 created


$ kubectl expose deployment web2 --port 80

service/web2 exposed


$ kubectl get svc

NAME                       TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)                      AGE
kubernetes                 ClusterIP      10.96.0.1       <none>        443/TCP                      26h
web1                       ClusterIP      10.99.135.62    <none>        80/TCP                       54s
web2                       ClusterIP      10.102.255.54   <none>        80/TCP                       19s
```

**2) それぞれのPodのnginx defaultページを書き換えて、どのPodにアクセスしたかが分かるようにします。それにはコンテナに接続して vim エディタをインストール後 /usr/share/nginx/html/index.html を書き換えます。**
```
$ kubectl get pods

NAME                                        READY   STATUS    RESTARTS   AGE
web1-c5d84d68d-vpqtg                        1/1     Running   0          116s
web1-c5d84d68d-wkh9d                        1/1     Running   0          116s
web2-6b9d85c467-5csf5                       1/1     Running   0          80s
web2-6b9d85c467-p8vrc                       1/1     Running   0          80s


$ kubectl exec -ti web1-c5d84d68d-vpqtg -- /bin/bash


root@web1-c5d84d68d-vpqtg:/# apt-get update

Get:1 http://deb.debian.org/debian bookworm InRelease [151 kB]
Get:2 http://deb.debian.org/debian bookworm-updates InRelease [52.1 kB]
Get:3 http://deb.debian.org/debian-security bookworm-security InRelease [48.0 kB]
Get:4 http://deb.debian.org/debian bookworm/main amd64 Packages [8780 kB]
Get:5 http://deb.debian.org/debian bookworm-updates/main amd64 Packages [6668 B]
Get:6 http://deb.debian.org/debian-security bookworm-security/main amd64 Packages [105 kB]
Fetched 9143 kB in 1s (6518 kB/s)
Reading package lists... Done


root@web1-c5d84d68d-vpqtg:/# apt-get install vim -y

Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
...


root@web1-c5d84d68d-vpqtg:/# vim /usr/share/nginx/html/index.html

<!DOCTYPE html>
<html>
<head>
<title>Internal Welcome Page</title>   #<-- この行を編集 <title>Web1111-AAA Internal Welcome Page</title>など

```

**3) Podからexitします。同様に他のPodも書き換えます。**
```
root@web1-c5d84d68d-vpqtg:/# exit
```

**4) Ingressリソースを作成します。マニフェストテンプレートを作成してから変更します。specにingressClassName: nginxを追加します。このIngressルールがNginx Ingress Controller用である事を定義しています。**
```
$ kubectl create ingress ingress-test --rule="www.web1.external.com/=web1:80" --dry-run=client -o yaml > ingress.yaml


$ vi ingress.yaml

spec:
  ingressClassName: nginx    #<-追加
  rules:

```

[ingress.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/07_Lab7/Labfiles/ingress.yaml)


**5) Ingressリソースを作成します。**
```
$ kubectl apply -f ingress.yaml

ingress.networking.k8s.io/ingress-test created
```

**6) 作成されたIngressリソースを確認します。**
```
$ kubectl get ingress

NAME           CLASS   HOSTS                   ADDRESS   PORTS   AGE
ingress-test   nginx   www.web1.external.com             80      55s
```

**7) Ingress controllerによりLoad Balancerタイプのサービスが作成されています。これがIngressの受け口になります。**
```
$ kubectl -n ingress get svc

NAME                       TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)                      AGE
nginx-ingress-controller   LoadBalancer   10.111.34.98    <pending>     80:30915/TCP,443:30014/TCP   23m
```

**8) アクセスしてみます。Ingressルールでホスト名での転送を設定したので偽装します。（単純にアクセスすると404になります。）**
```
$ curl -H "Host: www.web1.external.com" http://10.111.34.98

<!DOCTYPE html>
<html>
<head>
<title>web1111111-bbbbbbbbbb Welcome to nginx!</title>　←書き換えたタイトル
<省略>

```

**9) 何回かアクセスしてみましょう。2つのPodでロードバランスされている事が確認できます。**

**10) Ingressルールにweb2サービスも追加しましょう。マニフェストを更新します。**
```
$ vi ingress.yaml

以下を追加
  - host: www.web2.external.com
    http:
      paths:
      - backend:
          service:
            name: web2
            port:
              number: 80
        path: /
        pathType: Exact

```

[ingress.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/07_Lab7/Labfiles/ingress-2.yaml)

**11) 適用しましょう。kubectl apply は設定の更新ができますので再作成の必要はありません。**
```
$ kubectl apply -f ingress.yaml

ingress.networking.k8s.io/ingress-test configured

```

**12) ホスト名をwww.web2.external.comで確認します。web2サービスに転送された事が確認できます。**
```
$ curl -H "Host: www.web2.external.com" http://10.111.34.98

<!DOCTYPE html>
<html>
<head>
<title>web222222-bbbbbbbbb Welcome to nginx!</title>
...
```

**13) 何度か確認してロードバランスされている事をみてみましょう。**

**14) 作成したDeployment、Service、Ingressルールを削除しておきましょう。**
```
$ kubectl delete deployment web1
$ kubectl delete deployment web2
$ kubectl delete svc web1
$ kubectl delete svc web2
$ kubectl delete ingress ingress-test

```


<br>
<br>
以上でこのラボは終了です。

[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129)    
