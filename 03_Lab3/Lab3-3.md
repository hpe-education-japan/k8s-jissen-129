## Task 3:  .kube/configファイルの認証情報確認

.kube/configファイルから抽出した認証情報を使ってkubectlアクセスしてみます。クライアント証明書、キー情報、クラスター証明書はbase64エンコードされて.kube/configに格納されています。それぞれデコードして証明書、キーファイルを作成します。その後 .kube/config の無い状態で、マニュアルで証明書を指定してkubectlコマンドを実行してみます。  

<br>

**1) .kube/configの内容を確認しましょう。**
```
$ cat .kube/config

apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSU
...
```

**2) .kube/config内に格納されている認証情報(client-cert)を書き出します。**
```
$ echo $(grep client-cert $HOME/.kube/config |cut -d" " -f 6) > client


$ cat client

LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1...

```

**3) この値はbase64エンコードされています。デコードして.crtファイルを作成します。**
```
$ cat client | base64 --decode > client.crt

```

**4) 同様にクライアント鍵データ(client-key-data)を ファイルに書き出します。**
```
$ echo $(grep client-key-data $HOME/.kube/config |cut -d " " -f 6) > key


$ cat key

LS0tLS1CRUdJTiBSU0EgUFJJVkF...

```

**5) デコードして.keyファイルを作成します。**
```
$ cat key | base64 --decode > client.key 
```

**6) 同様にクラスタ認証のための鍵データ(certificate-authority-data) をファイルに書き出します。**
```
$ echo $(grep certificate-authority-data $HOME/.kube/config |cut -d " " -f 6) > auth


$ cat auth

LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS...

```

**7) デコードして.crtファイルを作成します。**
```
$ cat auth | base64 --decode > ca.crt
```

**8) server情報をserver変数に設定しましょう。**
```
$ export server=$(grep server $HOME/.kube/config |cut -d " " -f 6)


$ echo $server

https://160.251.204.109:6443
```
**9) .kube/configを参照しないように名前を変更にします。**
```
$ mv .kube/config .kube/config.save
```

**10) kubectlコマンドはエラーになります。**
```
$ kubectl get pods

E1127 14:16:20.351054   16452 memcache.go:265] couldn't get current server API group list: Get "http://localhost:8080/api?timeout=32s": dial tcp 127.0.0.1:8080: connect: connection refused

...
The connection to the server localhost:8080 was refused - did you specify the right host or port?
```

**11) 認証情報、キー、サーバー情報を手動で指定して実行します。**
```
$ kubectl --server $server --certificate-authority ca.crt --client-key client.key --client-certificate client.crt  get pods

NAME    READY   STATUS    RESTARTS   AGE
nginx   1/1     Running   0          20m

```

**12) .kube/configファイルを戻しておきましょう。作成したPodを削除しておきましょう。--allで全てのPodを削除できます。**
```
$ cp .kube/config.save .kube/config


$ kubectl get pods

NAME    READY   STATUS    RESTARTS   AGE
nginx   1/1     Running   0          21m


$ kubectl delete pods --all

pod "nginx" deleted
```

<br>
<br>
以上でこのタスクは終了です。

[Next](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/03_Lab3/Lab3-4.md)  
[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129)  

