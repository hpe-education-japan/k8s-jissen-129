## Task 4: kubectl configコマンドの確認

**1) kubectl config viewコマンドを使用して、現在のkubectl設定ファイルの内容を表示できます。これにより、設定ファイル内のクラスター、ユーザー、コンテキストなどの情報が表示されます。**
```
$ kubectl config view

apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: DATA+OMITTED
    server: https://k8scp:6443
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: kubernetes-admin
  name: kubernetes-admin@kubernetes
current-context: kubernetes-admin@kubernetes
kind: Config
preferences: {}
users:
- name: kubernetes-admin
  user:
    client-certificate-data: DATA+OMITTED
    client-key-data: DATA+OMITTED
```

**2) kubectl config current-contextを使用して、現在のコンテキスト（クラスター、ユーザー、Namespaceの組み合わせ）の名前を表示できます。**
```
$ kubectl config current-context

kubernetes-admin@kubernetes

```
<br>
<br>

**※項番3以降は演習では実行できません。HelpやDocument等で確認のみ行ってください。**  
<br>
**3) kubectl config use-contextを使用して、異なるコンテキストに切り替えることができます。コンテキストの切り替えにより、異なるクラスターや名前空間にアクセスできます。**

$ kubectl config use-context <context_name>  


**4) 新しいコンテキストの作成: kubectl config set-contextを使用して、新しいコンテキストを作成できます。これにはクラスター、ユーザー、名前空間などの情報を指定します。**

$ kubectl config set-context <context_name> --cluster=<cluster_name> --user=<user_name> \  
	--namespace=<namespace_name>

**5) kubectl config set-credentialsを使用して、新しいユーザー情報を設定できます。これには認証情報（トークン、ユーザー名とパスワードなど）を含めることができます。**

$ kubectl config set-credentials <user_name> --token=<token_value>

**6) kubectl config set-clusterを使用して、新しいクラスター情報を設定できます。これにはクラスターのサーバーエンドポイントや証明書情報などが含まれます。**

$ kubectl config set-cluster <cluster_name> --server=<server_url> --certificate-authority=<ca_cert_file>

**7) kubectl config setコマンドを使用して、設定ファイル内の各種情報を変更できます。たとえば、コンテキストの切り替え、ユーザーの変更、クラスターの変更などが含まれます。**

$ kubectl config set <PROPERTY_NAME> <PROPERTY_VALUE>

<br>
<br>
以上でこのラボは終了です。

[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129)
