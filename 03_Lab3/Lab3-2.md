## Task 2: マニフェストを使ったPodの作成
  
**1) Podマニフェストを作成します。**

```
$ vi pod.yaml

```
[pod.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/03_Lab3/pod.yaml)


**2) Podを作成して確認しましょう。**

```
$ kubectl create -f pod.yaml

pod/nginx created


$ kubectl get pods

NAME    READY   STATUS    RESTARTS   AGE
nginx   1/1     Running   0          19s

```

-o wideでPodのIPアドレスなどを確認できます。  
```
$ kubectl get pods -o wide

NAME    READY   STATUS    RESTARTS   AGE   IP                NODE           NOMINATED NODE   READINESS GATES
nginx   1/1     Running   0          90s   192.168.215.130   set99-worker   <none>           <none>

```

-o yamlでPodの設定をyaml形式で表示します。  
```
$ kubectl get pods -o yaml

apiVersion: v1
items:
- apiVersion: v1
  kind: Pod
  metadata:
    annotations:
...

```

describeでPodの詳細情報を表示します。  
```
$ kubectl describe pod nginx

apiVersion: v1
items:
- apiVersion: v1
  kind: Pod
  metadata:
    annotations:
...
```

Podを削除します。  
```
$ kubectl delete pod nginx

pod "nginx" deleted
```

**3) 今度はCLIでPodを作成しましょう。**

```
$ kubectl run --image=nginx nginx

pod/nginx created

$ kubectl get pods

NAME    READY   STATUS    RESTARTS   AGE
nginx   1/1     Running   0          23s

Podは残しておきます。
```

<br>
以上でこのタスクは終了です。

[Next](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/03_Lab3/Lab3-3.md)  
[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129)  
