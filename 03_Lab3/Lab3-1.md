# 3章 APIserver

**時間:20分**

**目的:**

このラボを完了すると、次のことができるようになります。  
・Kubectlコマンドを使ったリソース操作  
・kube/configファイルの理解  


**Notices:**

・Kubernetesはインストール済みです。  
・使用するUbuntuサーバーのログイン方法はインストラクターから提供します。  

<br>
<br>


## Task1: kubectl CLIの確認

**1) 各自アサインされたユーザーでサーバーにログインします**

**2) kubectl CLIをいくつか確認しましょう。\<Tab\>を使うとコマンド補完が使えるので確認してみてください。**

```
$ kubectl get pods

No resources found in default namespace.


$ kubectl get pods --all-namespaces

kube-system   cilium-8bdg8                       1/1     Running   1 (16m ago)   17h
kube-system   cilium-envoy-6wg67                 1/1     Running   1 (16m ago)   17h
kube-system   cilium-envoy-tjdxm                 1/1     Running   1 (16m ago)   17h
kube-system   cilium-operator-5c7867ccd5-9dj64   1/1     Running   1 (16m ago)   17h
kube-system   cilium-w9wbn                       1/1     Running   1 (16m ago)   17h
kube-system   coredns-7c65d6cfc9-52jfm           1/1     Running   1 (16m ago)   17h
kube-system   coredns-7c65d6cfc9-mhmhf           1/1     Running   1 (16m ago)   17h
kube-system   etcd-set0-cp                       1/1     Running   1 (16m ago)   17h
...


$ kubectl get pods -n kube-system

NAME                               READY   STATUS    RESTARTS      AGE
cilium-8bdg8                       1/1     Running   1 (19m ago)   17h
cilium-envoy-6wg67                 1/1     Running   1 (19m ago)   17h
cilium-envoy-tjdxm                 1/1     Running   1 (19m ago)   17h
cilium-operator-5c7867ccd5-9dj64   1/1     Running   1 (19m ago)   17h
cilium-w9wbn                       1/1     Running   1 (19m ago)   17h
coredns-7c65d6cfc9-52jfm           1/1     Running   1 (19m ago)   17h
coredns-7c65d6cfc9-mhmhf           1/1     Running   1 (19m ago)   17h
...


$ kubectl -n kube-system get pods kube-apiserver-<Tab>

NAME                          READY   STATUS    RESTARTS   AGE
kube-apiserver-set99-master   1/1     Running   0          6m33s


$ kubectl -n kube-system describe pod kube-apiserver-<Tab>

Name:                 kube-apiserver-set99-master
Namespace:            kube-system
Priority:             2000001000
Priority Class Name:  system-node-critical
Node:                 set99-master/10.0.0.253
Start Time:           Mon, 27 Nov 2023 13:36:57 +0900
Labels:               component=kube-apiserver
                      tier=control-plane
...
```
<br>
<br>
以上でこのタスクは終了です。

[Next](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/03_Lab3/Lab3-2.md)  
[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129)  

