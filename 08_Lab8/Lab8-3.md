## Task3 : TaintとToleration

ワーカーノードにTaintを設定してPodの起動を禁止し、Toleration設定のあるPodだけを起動するようにします。  
<br>
<br>
![Lab8-1](/uploads/a3fd1a53a0e166ca88f920035f7d5425/Lab8-1.png)
<br>
<br>
**1) NodeにTaint設定があるかを確認します。今回の演習環境は作成時にコントロールプレーンノードにデフォルトで付加されるTaintを削除しています。通常はコントロールプレーンノードでPodが起動しないようにTaintが付加されていますので注意してください。**
```
$ kubectl describe node | grep -i taint

Taints:             <none>
Taints:             <none>
```

**2) Podを起動します**
```
$ kubectl run nginx --image nginx

pod/nginx created


$ kubectl get pods -o wide
NAME                                        READY   STATUS    RESTARTS   AGE   IP                NODE           NOMINATED NODE   READINESS GATES
nginx                                       1/1     Running   0          16s   192.168.215.180   set99-worker   <none>           <none>
```

**3) Podが稼働しているNodeにTaintを設定します。EffectをNoExecuteにします。**
```
$ kubectl taint node setXX-worker app=prod:NoExecute

node/set99-worker tainted


$ kubectl describe node | grep -i taint

Taints:             <none>
Taints:             app=prod:NoExecute
```

**4) 起動中のPodはどうなったでしょうか。**
```
$ kubectl get pods

No resources found in default namespace.
```

**5) 再度新規Podを作成してみます。Tolerationは無しで起動してみましょう。**
```
$ kubectl run nginx2 --image=nginx

pod/nginx2 created
```

**6) Podの状態を確認しましょう。Taintを付加していないNodeで起動しています。**
```
$ kubectl get pods -o wide

NAME                                        READY   STATUS    RESTARTS   AGE     IP               NODE           NOMINATED NODE   READINESS GATES
nginx2                                      1/1     Running   0          81s     192.168.108.18   set99-cp       <none>           <none>
```

**7) ワーカーノードで実行するPodを作成しましょう。Tolerationを付けてNodeSelectorを併用します。マニフェストを作成します。**
```
$ vi nginx3.yaml
```

[nginx3.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/08_Lab8/Labfiles/nginx3.yaml)

```
$ kubectl create -f nginx3.yaml

pod/nginx3 created
```

**8) 起動したか確認します。まだNodeにLabelが無いのでPendingです。**
```
$ kubectl get pods -o wide

NAME                                        READY   STATUS    RESTARTS   AGE     IP               NODE           NOMINATED NODE   READINESS GATES
nginx2                                      1/1     Running   0          4m44s   192.168.108.18   set99-cp       <none>           <none>
nginx3                                      0/1     Pending   0          41s     <none>           <none>         <none>           <none>
```

**9) Taintを付加しているワーカーノードにLabelを設定しましょう。**
```
$ kubectl label node setXX-worker env=dev

node/setXX-worker labeled
```

**10) Podを確認しましょう。ワーカーノードで起動しています。ワーカーノードはToleration付きのPodのみ起動が可能です。**
```
$ kubectl get pods -o wide

NAME                                        READY   STATUS    RESTARTS   AGE     IP                NODE           NOMINATED NODE   READINESS GATES
nginx2                                      1/1     Running   0          6m27s   192.168.108.18    set99-cp       <none>           <none>
nginx3                                      1/1     Running   0          2m24s   192.168.215.181   set99-worker   <none>           <none>
```

**11) NodeからTaintとLabelを削除しましょう。削除するには「-」を使います。**
```
$ kubectl taint node setXX-worker app=prod:NoExecute-
$ kubectl label node setXX-cp env-
$ kubectl label node setXX-worker env-

```

**12) Podを削除しましょう。**
```
$ kubectl delete pods nginx2
$ kubectl delete pods nginx3
```


<br>
<br>
以上でこのラボは終了です。

[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129)    

