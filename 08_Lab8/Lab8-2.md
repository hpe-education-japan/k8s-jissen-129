## Task2 : PodAffinityを使ったPodの配置

**1) PodAffiniyを設定したPodを作成します。設定は以下のようにします。**
```
$ vi pod-pa.yaml

PodAffinity設定
spec:
  containers:
    - name: my-container-2
      image: nginx
  affinity:
    podAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        - labelSelector:
            matchExpressions:
              - key: group
                operator: In
                values:
                  - one
          topologyKey: kubernetes.io/hostname

```

[pod-pa.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/08_Lab8/Labfiles/pod-pa.yaml)

```
$ kubectl apply -f pod-pa.yaml

pod/my-pod-2 created


$ kubectl get pods

NAME                                        READY   STATUS    RESTARTS   AGE
my-pod                                      1/1     Running   0          8m27s
my-pod-2                                    0/1     Pending   0          23s

PodAffinityで設定したラベルを持つPodが存在しないためPendingステータスです。
```

**2) この前の演習で作成したPodにラベルを追加します。**
```
$ kubectl label pod my-pod group=one

pod/my-pod labeled
```

**3) Podを確認します。同じNodeで起動しました。**
```
$ kubectl get pods -o wide

NAME                                        READY   STATUS    RESTARTS   AGE     IP                NODE           NOMINATED NODE   READINESS GATES
my-pod                                      1/1     Running   0          9m57s   192.168.108.13    set99-cp      <none>           <none>
my-pod-2                                    1/1     Running   0          113s    192.168.108.14    set99-cp      <none>           <none>
```

**4) Podを削除しておきます。**
```
$ kubectl delete pod my-pod
$ kubectl delete pod my-pod-2

```


<br>
<br>
以上でこのタスクは終了です。

[Next](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/08_Lab8/Lab8-3.md)  
[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129)    

