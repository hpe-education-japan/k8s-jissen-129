# 8章 Scheduling

**時間:30分**

**目的:**

このラボを完了すると、次のことができるようになります。  
・NodeSelectorを使う  
・PodAffinityを使う  
・TaintとTolerationを使う  


<br>
<br>


## Task1: NodeSelectorを使ったPodの配置

**1) 各NodeのLabelを確認します。**
```
$ kubectl get node --show-labels

NAME           STATUS   ROLES           AGE   VERSION   LABELS
set99-cp       Ready    control-plane   26h   v1.27.1   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=set99-cp,kubernetes.io/os=linux,node-role.kubernetes.io/control-plane=,node.kubernetes.io/exclude-from-external-load-balancers=
set99-worker   Ready    <none>          26h   v1.27.1   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=set99-worker,kubernetes.io/os=linux
```

**2) NodeSelectorを設定して env=prod のLabelの付いたNodeで起動するPodを作成します。**
```
$ vi pod-node.yaml

```
[pod-node.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/08_Lab8/Labfiles/pod-node.yaml)

```
$ kubectl create -f pod-node.yaml

pod/my-pod created


$ kubectl get pods

NAME                                        READY   STATUS    RESTARTS   AGE
my-pod                                      0/1     Pending   0          31s
```

**3) PodはPendingのままです。理由を調べます。**
```
$ kubectl describe pod my-pod

...
Events:
  Type     Reason            Age   From               Message
  ----     ------            ----  ----               -------
  Warning  FailedScheduling  2m    default-scheduler  0/2 nodes are available: 2 node(s) didn't match Pod's node affinity/selector. preemption: 0/2 nodes are available: 2 Preemption is not helpful for scheduling..
```

**4) コントロールプレーンノードにラベルを設定します。**
```
$ kubectl label node setXX-cp env=prod

node/setXX-cp labeled
```

**5) Podステータスを調べます。コントロールプレーンノードで起動してRunningになりました。**
```
$ kubectl get pods -o wide

NAME                                        READY   STATUS    RESTARTS   AGE     IP                NODE           NOMINATED NODE   READINESS GATES
my-pod                                      1/1     Running   0          3m51s   192.168.108.13    set99-cp   <none>           <none>
```


<br>
<br>
以上でこのタスクは終了です。

[Next](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/08_Lab8/Lab8-2.md)  
[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129)    
