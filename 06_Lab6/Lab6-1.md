# 6章 Service

**時間:15分**

**目的:**

このラボを完了すると、次のことができるようになります。  
・Serviceを作成する  


<br>
<br>


## Task1: サービスを作成する

**1) nginx Podをデプロイします。**
```
$ kubectl create deployment nginx --image=nginx --replicas=3

deployment.apps/nginx created


$ kubectl get pods

NAME                     READY   STATUS    RESTARTS   AGE
nginx-77b4fdf86c-h5vns   1/1     Running   0          28s
nginx-77b4fdf86c-nxrb5   1/1     Running   0          28s
nginx-77b4fdf86c-xtv6b   1/1     Running   0          28s
```

**2) exposeコマンドでサービスを作成します。デフォルトのClusterIPサービスを作成します。**
```
$ kubectl expose deployment nginx --port 80

service/nginx exposed
```

**3) serviceを確認しましょう。**
```
$ kubectl get svc

NAME         TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1        <none>        443/TCP   25h
nginx        ClusterIP   10.108.127.113   <none>        80/TCP    43s
```

**4) 動作を確認します。**
```
$ curl 10.108.127.113

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
...
```

**5) Serviceのエンドポイントを見てみましょう。**
```
$ kubectl get ep

NAME         ENDPOINTS                                                 AGE
kubernetes   10.0.0.253:6443                                           25h
nginx        192.168.108.10:80,192.168.215.175:80,192.168.215.176:80   2m28s
```

**6) サービスを削除しておきます。**
```
$ kubectl delete svc nginx

service "nginx" deleted

```

**7) 次にNodePortタイプのサービスを作成します。Serviceの名前も指定しましょう。**
```
$ kubectl expose deployment nginx --name=nginx-nodeport --port=80 --type=NodePort

service/nginx-nodeport exposed
```

**8) 確認しましょう。**
```
$ kubectl get svc

NAME             TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
kubernetes       ClusterIP   10.96.0.1       <none>        443/TCP        25h
nginx-nodeport   NodePort    10.110.148.13   <none>        80:30075/TCP   36s


$ kubectl describe svc nginx-nodeport

Name:                     nginx-nodeport
Namespace:                default
Labels:                   app=nginx
Annotations:              <none>
Selector:                 app=nginx
Type:                     NodePort
IP Family Policy:         SingleStack
IP Families:              IPv4
IP:                       10.110.148.13
IPs:                      10.110.148.13
Port:                     <unset>  80/TCP
TargetPort:               80/TCP
NodePort:                 <unset>  30075/TCP
Endpoints:                192.168.108.10:80,192.168.215.175:80,192.168.215.176:80
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>
```

**9) ホストのIPとPort番号で接続してみましょう。**
```
$ curl localhost:30075

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
...
```

**10) 他のメンバーノードのIPでも確認してみましょう。**


**11) ServiceとDeploymentを削除しておきます。**
```
$ kubectl delete svc nginx-nodeport
$ kubectl delete deployment nginx

```

**12) 課題演習：NodePortサービスを、マニフェストを使って作成してみましょう。**


<br>
<br>
以上でこのラボは終了です。

[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129)    
