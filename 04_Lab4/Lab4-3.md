## Task 3: DaemonSetを作成する

**1) Daemonsetのマニフェストを作成します。その後apply -f で作成します。**
```
$ vi ds.yaml 
```
[ds.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/04_Lab4/Labfiles/ds.yaml)

```
$ kubectl apply -f ds.yaml

daemonset.apps/nginx-daemonset created

```

**2) 作成したDaemonSetを確認してみましょう。各ノードでPodが動いています。**
```
$ kubectl get ds

NAME              DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
nginx-daemonset   2         2         2       2            2           <none>          37s


$ kubectl describe ds nginx-daemonset

Name:           nginx-daemonset
Selector:       app=nginx
Node-Selector:  <none>
Labels:         app=nginx
Annotations:    deprecated.daemonset.template.generation: 1
...


$ kubectl get pods -o wide

NAME                    READY   STATUS    RESTARTS   AGE   IP                NODE           NOMINATED NODE   READINESS GATES
nginx-daemonset-2rp5r   1/1     Running   0          97s   192.168.215.142   set99-worker   <none>           <none>
nginx-daemonset-c4thb   1/1     Running   0          97s   192.168.108.7     set99-cp       <none>           <none>
```

**3) DaemonSetを削除しておきましょう。**
```
$ kubectl delete ds nginx-daemonset

daemonset.apps "nginx-daemonset" deleted

```

<br>
<br>
以上でこのタスクは終了です。  

[Next](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/04_Lab4/Lab4-4.md)  
[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129)  



```
