## Task 5: CronJobを作成する

**1) CronJobマニフェストを作成します。kubectl create cronjob コマンドを使いましょう。1分ごとに起動するように設定しましょう。**
```
$ kubectl create cronjob my-job --dry-run=client --image=busybox --schedule="*/1 * * * *" -o yaml -- sleep 10 > cronjob.yaml


$ cat cronjob.yaml

apiVersion: batch/v1
kind: CronJob
metadata:
  creationTimestamp: null
  name: my-job
spec:
  jobTemplate:
    metadata:
      creationTimestamp: null
      name: my-job
    spec:
      template:
        metadata:
          creationTimestamp: null
        spec:
          containers:
          - command:
            - sleep
            - "10"
            image: busybox
            name: my-job
            resources: {}
          restartPolicy: OnFailure
  schedule: '*/1 * * * *'
status: {}
```

**2) CronJobを作成しましょう。**
```
$ kubectl apply -f cronjob.yaml

cronjob.batch/my-job created
```

**3) Cronjob、Job、Podのステータスを確認しましょう。時間をおいて何度か確認して下さい。タイミングで表示が異なるかもしれません。**
```
$ kubectl get cronjobs.batch

NAME     SCHEDULE      SUSPEND   ACTIVE   LAST SCHEDULE   AGE
my-job   */1 * * * *   False     0        <none>          8s


$ kubectl get job

No resources found in default namespace.


時間をおいてjobとpodをのステータスを確認します。１分間隔で実行されています。


$ kubectl get job

NAME              COMPLETIONS   DURATION   AGE
my-job-28352224   1/1           15s        20s


$ kubectl get pods

NAME                    READY   STATUS      RESTARTS   AGE
my-job-28352228-t8m8b   0/1     Completed   0          25s


更に時間をおいてjobとpodをのステータスを確認します。


$ kubectl get job

 NAME              COMPLETIONS   DURATION   AGE
my-job-28352230   1/1           14s        2m38s
my-job-28352231   1/1           14s        98s
my-job-28352232   1/1           18s        38s


$ kubectl get pods

NAME                    READY   STATUS      RESTARTS   AGE
my-job-28352231-p77c2   0/1     Completed   0          2m44s
my-job-28352232-mbfcd   0/1     Completed   0          104s
my-job-28352233-8cxkk   0/1     Completed   0          44s

```

**4) cronJobを削除しましょう。**
```
$ kubectl delete cronjobs.batch my-job

cronjob.batch "my-job" deleted
```


<br>
<br>
以上でこのラボは終了です。 

[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129)

