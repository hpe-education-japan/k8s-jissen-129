# 4章 Deployment

**時間:40分**

**目的:**

このラボを完了すると、次のことができるようになります。  
・Deploymentを作成し、操作する  
・ローリングアップデートを行う  
・DaemonSetを作成する  
・Jobを作成する  
・CronJobを作成する  


<br>
<br>


## Task1: Deploymentの操作

<br>
![Lab4-1](/uploads/a18c8302d71f7198fcecc4f7ef47e76b/Lab4-1.png)

<br>
<br>


**1) kubectl CLIでDeploymentを作成します。レプリカ数を3にします。**
```
$ kubectl create deployment my-app --image=nginx:latest --replicas=3

deployment.apps/my-app created

```

**2) 作成されたDeploymentが管理するReplicasetとPodを確認します。名前を注目してください。**
```
$ kubectl get deployment,rs,pod

NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/my-app   3/3     3            3           24s

NAME                               DESIRED   CURRENT   READY   AGE
replicaset.apps/my-app-cc4bd9d4d   3         3         3       24s

NAME                         READY   STATUS    RESTARTS   AGE
pod/my-app-cc4bd9d4d-5wvs9   1/1     Running   0          10m
pod/my-app-cc4bd9d4d-7mfjg   1/1     Running   0          10m
pod/my-app-cc4bd9d4d-jp6r9   1/1     Running   0          10m

```

**3) kubectl describeでlabel selectorやPod labelを確認します。**
```
$ kubectl describe deployments.apps my-app

Name:                   my-app
Namespace:              default
CreationTimestamp:      Thu, 19 Oct 2023 01:18:40 -0400
Labels:                 app=my-app
Annotations:            deployment.kubernetes.io/revision: 1
Selector:               app=my-app
Replicas:               3 desired | 3 updated | 3 total | 3 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  		          app=my-app
  Containers:
   nginx:
    Image:        	    nginx:latest
    Port:         	    <none>
    Host Port:    	    <none>
    Environment:  	    <none>
    Mounts:       	    <none>
  Volumes:        	    <none>
Conditions:
  Type 	         Status  Reason
   ----          ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   my-app-cc4bd9d4d (3/3 replicas created)
Events:
  Type    Reason             Age    From                   Message
  ----    ------             ----   ----                   -------
  Normal  ScalingReplicaSet  4m31s  deployment-controller  Scaled up replica set my-app-cc4bd9d4d to 3
```
```
$ kubectl describe pod my-app-cc4bd9d4d-5wvs9

Name:             	my-app-cc4bd9d4d-5wvs9
Namespace:        	default
Priority:         	0
Service Account:  	default
Node:             	set99-cp/10.0.0.73
Start Time:       	Wed, 25 Oct 2023 13:51:21 +0900
Labels:           	app=my-app2
<省略>
Status:           	Running
IP:               	192.168.108.8
IPs:
  IP:  192.168.108.8
Containers:
  nginx:
    Container ID:   containerd://be412e54ca9c0ad7b685002246905b71baf28638130c5a9941b459ead656cf29
    Image:          nginx:latest
<省略>

```

**4) Podを1つ削除してみましょう。Podの数は一定に保たれています。**
```
$ kubectl delete pod my-app-cc4bd9d4d-5wvs9

pod "my-app-cc4bd9d4d-5wvs9" deleted


$ kubectl get pods
NAME                     READY   STATUS    RESTARTS   AGE
my-app-cc4bd9d4d-7mfjg   1/1     Running   0          20m
my-app-cc4bd9d4d-97lwl   1/1     Running   0          26s
my-app-cc4bd9d4d-jp6r9   1/1     Running   0          20m
```

**5) kubectl edit を使って1つのPodのLabelを変えてみましょう。その後Podの数を確認してみてください。--show-labelsでラベルを表示できます。**
```
$ kubectl edit pod my-app-cc4bd9d4d-7mfjg

  labels:
    app: my-app2 #<-Labelを変更
```
```
$ kubectl get pods --show-labels

NAME                     READY   STATUS    RESTARTS   AGE     LABELS
my-app-cc4bd9d4d-7mfjg   1/1     Running   0          22m     app=my-app2,pod-template-hash=cc4bd9d4d
my-app-cc4bd9d4d-97lwl   1/1     Running   0          2m42s   app=my-app,pod-template-hash=cc4bd9d4d
my-app-cc4bd9d4d-jp6r9   1/1     Running   0          22m     app=my-app,pod-template-hash=cc4bd9d4d
my-app-cc4bd9d4d-prf2n   1/1     Running   0          6s      app=my-app,pod-template-hash=cc4bd9d4d
```

**6) Deploymentを削除します。**
```
$ kubectl delete deployments.apps my-app

deployment.apps "my-app" deleted

```

**7) Podを確認します。Podは1つ残っています。このPodはDepploymentの管理外になりました。Podは削除しておきましょう。**
```
$ kubectl get pods --show-labels

NAME                     READY   STATUS    RESTARTS   AGE      LABELS
my-app-cc4bd9d4d-7mfjg   1/1     Running   0          22m      app=my-app2,pod-template-hash=cc4bd9d4d


$ kubectl delete pod my-app-cc4bd9d4d-7mfjg

pod "my-app-cc4bd9d4d-7mfjg" deleted

```

**8) 次にDeploymentのマニフェストを作成します。--dry-runと-o yamlオプションを使いましょう。名前をmy-app2にします。**
```
$ kubectl create deployment my-app2 --image=nginx:latest --replicas=3 --dry-run=client -o yaml > deployment.yaml
```

**9) 作成されたマニフェストを確認して、Deploymentを作成しましょう。**
```
$ cat deployment.yaml

apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: my-app2
  name: my-app2
spec:
  replicas: 3
  selector:
    matchLabels:
      app: my-app2
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: my-app2
    spec:
      containers:
      - image: nginx:latest
        name: nginx
        resources: {}
status: {}


$ kubectl create -f deployment.yaml

deployment.apps/my-app2 created
```

**10) レプリカ数を変更してみます。レプリカ設定はReplicaSetでも可能なので、まずReplicaSetで設定してみます。**
```
$ kubectl scale replicaset my-app2-58dd76dd9d --replicas 5

replicaset.apps/my-app2-58dd76dd9d scaled
```

**11) Podの数を確認します。3つのままです。ReplicaSetではPodを5に増やしましたが、上位のDeploymentでは設定が3のままなので元に戻りました。**
```
$ kubectl get pod

NAME                       READY   STATUS    RESTARTS   AGE
my-app2-58dd76dd9d-42584   1/1     Running   0          113s
my-app2-58dd76dd9d-pfxxn   1/1     Running   0          113s
my-app2-58dd76dd9d-v8hj8   1/1     Running   0          113s

```

**12) Deployment設定でレプリカ数を変更してみます。今度は数が増えました。**
```
$ kubectl scale deployment my-app2 --replicas 5

deployment.apps/my-app2 scaled


$ kubectl get pods

NAME                       READY   STATUS    RESTARTS   AGE
my-app2-58dd76dd9d-42584   1/1     Running   0          2m51s
my-app2-58dd76dd9d-m9fgt   1/1     Running   0          13s
my-app2-58dd76dd9d-pfxxn   1/1     Running   0          2m51s
my-app2-58dd76dd9d-v8hj8   1/1     Running   0          2m51s
my-app2-58dd76dd9d-v8w97   1/1     Running   0          13s
```

**13) Deploymentを削除しましょう。**
```
$ kubectl delete deployments.apps my-app2

deployment.apps "my-app2" deleted
```
<br>
<br>
以上でこのタスクは終了です。  

[Next](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/04_Lab4/Lab4-2.md)  
[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129)  
