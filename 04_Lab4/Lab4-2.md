## Task 2: Rolling Update

**1) --dry-runオプションを使ってマニフェストを作成してDeploymentを作成しましょう。イメージはnginx:1.16を使います。また、今回はkubectl apply -f を使って作成します。**
```
$ kubectl create deployment web --image=nginx:1.16 --dry-run=client -o yaml > web.yaml


$ kubectl apply -f web.yaml

deployment.apps/web created

```

**2) Deploymentの情報を表示してUpdate Strategyやイメージバージョンを確認します。**
```
$ kubectl describe deployments.apps web

Name:                   web
Namespace:              default
CreationTimestamp:      Mon, 27 Nov 2023 15:54:32 +0900
Labels:                 app=web
Annotations:            deployment.kubernetes.io/revision: 1
Selector:               app=web
Replicas:               1 desired | 1 updated | 1 total | 1 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=web
  Containers:
   nginx:
    Image:        nginx:1.16

...
```

**3) kubectl rollout statusコマンドを確認します。**
```
$ kubectl rollout status deployment web

deployment "web" successfully rolled out

```

**4) マニフェストを変更して、apply -fで適用し、イメージを1.17へバージョンアップします。**
```
$ vi web.yaml
```

[web.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/04_Lab4/Labfiles/web.yaml)

```
$ kubectl apply -f web.yaml

deployment.apps/web configured
```

**5) イメージバージョンを確認します。アップデートが成功しました。**
```
$ kubectl describe deployments.apps web

Name:                   web
Namespace:              default
CreationTimestamp:      Mon, 27 Nov 2023 15:54:32 +0900
Labels:                 app=web
Annotations:            deployment.kubernetes.io/revision: 2
Selector:               app=web
Replicas:               1 desired | 1 updated | 1 total | 1 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=web
  Containers:
   nginx:
    Image:        nginx:1.17
...
```
<br>
<br>

![Lab4-2](/uploads/763c32a814b62f6cfaf3cd538c5ecc16/Lab4-2.png)


<br>
<br>

**6) rollout statusやhistoryを確認します。**
```
$ kubectl rollout status deployment web

deployment "web" successfully rolled out


$ kubectl rollout history deployment web

deployment.apps/web
REVISION  CHANGE-CAUSE
1         <none>
2         <none>


$ kubectl rollout history deployment web --revision 1

deployment.apps/web with revision #1
Pod Template:
  Labels:       app=web
        pod-template-hash=796f986c69
  Containers:
   nginx:
    Image:      nginx:1.16
    Port:       <none>
    Host Port:  <none>
    Environment:        <none>
    Mounts:     <none>
  Volumes:      <none>


$ kubectl rollout history deployment web --revision 2

deployment.apps/web with revision #2
Pod Template:
  Labels:       app=web
        pod-template-hash=85c9896f8d
  Containers:
   nginx:
    Image:      nginx:1.17
    Port:       <none>
    Host Port:  <none>
    Environment:        <none>
    Mounts:     <none>
  Volumes:      <none>
```

**7) Rollbackしてみます。**

<br>
<br>

![Lab4-3](/uploads/ff511851ef6675ea3cae55b1bc13b444/Lab4-3.png)

<br>
<br>

```
$ kubectl rollout undo deployment web

deployment.apps/web rolled back


$ kubectl rollout status deployment web

deployment "web" successfully rolled out


$ kubectl rollout history deployment web

deployment.apps/web
REVISION  CHANGE-CAUSE
2         <none>
3         <none>


$ kubectl rollout history deployment web --revision 2

deployment.apps/web with revision #2
Pod Template:
  Labels:       app=web
        pod-template-hash=85c9896f8d
  Containers:
   nginx:
    Image:      nginx:1.17
    Port:       <none>
    Host Port:  <none>
    Environment:        <none>
    Mounts:     <none>
  Volumes:      <none>


$ kubectl rollout history deployment web --revision 3

deployment.apps/web with revision #3
Pod Template:
  Labels:       app=web
        pod-template-hash=796f986c69
  Containers:
   nginx:
    Image:      nginx:1.16
    Port:       <none>
    Host Port:  <none>
    Environment:        <none>
    Mounts:     <none>
  Volumes:      <none>

```

**8) イメージバージョンを確認します。**
```
$ kubectl describe deployments.apps web

Name:                   web
Namespace:              default
CreationTimestamp:      Mon, 27 Nov 2023 15:54:32 +0900
Labels:                 app=web
Annotations:            deployment.kubernetes.io/revision: 3
Selector:               app=web
Replicas:               1 desired | 1 updated | 1 total | 1 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=web
  Containers:
   nginx:
    Image:        nginx:1.16
...

```

**9) Deploymentを削除しましょう。**
```
$ kubectl delete deployments.apps web

deployment.apps "web" deleted
```

<br>
<br>
以上でこのタスクは終了です。  

[Next](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/04_Lab4/Lab4-3.md)  
[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129)  
