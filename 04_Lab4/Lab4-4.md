## Task 4: Jobを作成する

**1) 10秒間sleepするJobを作成します。kubectl create job で--dry-runオプションを使ってマニフェストを作成しましょう。**
```
$ kubectl create job my-job --dry-run=client --image=busybox -o yaml -- sleep 10 > job.yaml


$ cat job.yaml

apiVersion: batch/v1
kind: Job
metadata:
  creationTimestamp: null
  name: my-job
spec:
  template:
    metadata:
      creationTimestamp: null
    spec:
      containers:
      - command:
        - sleep
        - "10"
        image: busybox
        name: my-job
        resources: {}
      restartPolicy: Never
status: {}

```

**2) Jobを起動します。10秒経過する前と後でJobやPodのステータスを確認しましょう。**
```
$ kubectl apply -f job.yaml

job.batch/my-job created


$ kubectl get jobs

NAME     COMPLETIONS   DURATION   AGE
my-job   0/1           2s        2s


$ kubectl get pods

NAME           READY   STATUS      RESTARTS   AGE
my-job-tjm7h   1/1     Running     0          12s

10秒経過後

$ kubectl get jobs

NAME     COMPLETIONS   DURATION   AGE
my-job   1/1           14s        17s

  
$ kubectl get pods

NAME           READY   STATUS      RESTARTS   AGE
my-job-tjm7h   0/1     Completed   0          21s

```

**3) Jobを削除しておきましょう。**
```
$ kubectl delete jobs my-job

job.batch "my-job" deleted
```

**4) completionsパラメータを使って実行回数を増やしてみましょう。**
```
$ vi job.yaml
```

[job.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/04_Lab4/Labfiles/job.yaml)


**5) Jobを再作成します。**
```
$ kubectl apply -f job.yaml

job.batch/my-job created

```

**6) Jobのステータスを確認します。COMPLETIONS が 0/5 から始まります。実行中に何度かステータスを確認してみましょう。最終的に5/5で完了します。**
```
$ kubectl get jobs

NAME     COMPLETIONS   DURATION   AGE
my-job   0/5           5s         5s


$ kubectl get pods

NAME           READY   STATUS      RESTARTS   AGE
my-job-2ml6v   1/1     Running     0          8s


$ kubectl get jobs

NAME     COMPLETIONS   DURATION   AGE
my-job   2/5           28s        28s


$ kubectl get pods

NAME           READY   STATUS      RESTARTS   AGE
my-job-pf8cr   1/1     Running     0          6s
my-job-2ml6v   0/1     Completed   0          33s
my-job-lr4w5   0/1     Completed   0          19s


Job完了


$ kubectl get jobs

NAME     COMPLETIONS   DURATION   AGE
my-job   5/5           70s        4m6s


$ kubectl get pods

NAME           READY   STATUS      RESTARTS   AGE
my-job-2ml6v   0/1     Completed   0          4m5s
my-job-hp848   0/1     Completed   0          3m50s
my-job-lr4w5   0/1     Completed   0          4m32s
my-job-ng5m9   0/1     Completed   0          4m18s
my-job-pf8cr   0/1     Completed   0          3m36s

```

**7) Jobを削除しましょう。**
```
$ kubectl delete jobs my-job

job.batch "my-job" deleted
```

**8) マニフェストでparallelismパラメータを追加してみましょう。**
```
$ vi job.yaml
```
[job.yaml](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/04_Lab4/Labfiles/job2.yaml)

<br>

**9) Jobを作成します。**
```
$ kubectl apply -f job.yaml

job.batch/my-job created


$ kubectl get job

NAME     COMPLETIONS   DURATION   AGE
my-job   0/5           4s        4s
```

**10) kubectl get podsで確認するとPodが2個確認できます。タイミングによって数が異なるかもしれません。**
```
$ kubectl get pods

NAME           READY   STATUS      RESTARTS   AGE
my-job-28cqq   1/1     Running     0          8s
my-job-66nbc   1/1     Running     0          8s

```

**11) 何度かjobやPodのステータスを確認しましょう。最終的に5/5で終了します。**
```
$ kubectl get job

NAME     COMPLETIONS   DURATION   AGE
my-job   5/5           43s        45s


$ kubectl get pods

NAME           READY   STATUS      RESTARTS   AGE
my-job-28cqq   0/1     Completed   0          42s
my-job-66nbc   0/1     Completed   0          14s
my-job-86czd   0/1     Completed   0          27s
my-job-8n8pr   0/1     Completed   0          27s
my-job-bxzf2   0/1     Completed   0          42s

```

**12) Jobを削除しておきます。**
```
$ kubectl delete jobs my-job

job.batch "my-job" deleted
```

<br>
<br>
以上でこのタスクは終了です。

[Next](https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/04_Lab4/Lab4-5.md)  
[Top](https://gitlab.com/hpe-education-japan/k8s-jissen-129)    




