# Lab3 APIserver

### Lab3-Task1 kubectl CLIの確認,Task2 マニフェストを使ったPodの作成
https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/03_Lab3/Lab3-1.md?ref_type=heads

### Lab3-Task3 .kube/configファイルの認証情報確認

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/03_Lab3/Lab3-3.md?ref_type=heads

### Lab3-Task4 kubectl configコマンドの確認

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/03_Lab3/Lab3-4.md?ref_type=heads

<br>  

# Lab4 Deployment

### Lab4-Task1 Deploymentの操作

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/04_Lab4/Lab4-1.md?ref_type=heads

### Lab4-Task2 Rolling Update

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/04_Lab4/Lab4-2.md?ref_type=heads

### Lab4-Task3 DaemonSetを作成する

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/04_Lab4/Lab4-3.md?ref_type=heads

### Lab4-Task4 Jobを作成する

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/04_Lab4/Lab4-4.md?ref_type=heads

### Lab4-Task4 CronJobを作成する

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/04_Lab4/Lab4-5.md?ref_type=heads

<br>  

# Lab5 Volume

### Lab5-Task1 emptyDirを使ったPod内コンテナのファイル共有

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/05_Lab5/Lab5-1.md?ref_type=heads

### Lab5-Task2 hostPath Volume

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/05_Lab5/Lab5-2.md?ref_type=heads

### Lab5-Task3 PersistentVolumeとPersistentVolumeClaim

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/05_Lab5/Lab5-3.md?ref_type=heads

### Lab5-Task4 ConfigMapを使う

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/05_Lab5/Lab5-4.md?ref_type=heads

### Lab5-Task5 Secretを使う

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/05_Lab5/Lab5-5.md?ref_type=heads

### Lab5-Task6 Local Storageを使ったDynamic Provisioningの例

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/05_Lab5/Lab5-6.md?ref_type=heads

<br> 

# LAb6 Service

### Lab6-Task1 サービスを作成する

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/06_Lab6/Lab6-1.md?ref_type=heads

<br>  

# Lab7 Ingress

### Lab7-Task1 Ingress Controllerのインストール

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/07_Lab7/Lab7-1.md?ref_type=heads

### Lab7-Task2 Ingress の利用

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/07_Lab7/Lab7-2.md?ref_type=heads

<br>  

# Lab8 Scheduling

### Lab8-Task1 NodeSelectorを使ったPodの配置

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/08_Lab8/Lab8-1.md?ref_type=heads

### Lab8-Task2 Deployment

https://gitlab.com/hpe-education-japan/docker_and_kubernetes/-/blob/master/Lab8/Lab8-2.md

### Lab8-Task2 PodAffinityを使ったPodの配置

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/08_Lab8/Lab8-2.md?ref_type=heads

### Lab8-Task3 TaintとToleration

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/08_Lab8/Lab8-3.md?ref_type=heads

<br>  

# Lab9 Security

### Lab9-Task1 認証設定と認可設定

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/09_Lab9/Lab9-1.md?ref_type=heads

### Lab9-Task2 PodSecurity設定

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/09_Lab9/Lab9-2.md?ref_type=heads

### Lab9-Task3 Network Security Policy設定

https://gitlab.com/hpe-education-japan/k8s-jissen-129/-/blob/master/09_Lab9/Lab9-3.md?ref_type=heads


---

